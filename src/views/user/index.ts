import {Component, Vue} from 'vue-property-decorator';
import {mapGetters, mapActions} from 'vuex';
import {Loading} from 'element-ui';
import { UserModule } from '@/core/store/modules/user';

@Component({
  name: 'User',
  computed: {
    ...mapGetters({
      getUserById: 'getUserById'
    })
  }
})
export default class User extends Vue {
  public getUserById: any;
  public getUsers: any;
  public getUserAlbums: any;

  public data() {
    return {
      user: ''
    };
  }

  public async mounted() {
    await this.getUser(+this.$route.params.id);
  }

  private async getUser(id: number) {
    const loading = Loading.service({ fullscreen: true });
    this.$data.user = this.getUserById(id);
    console.log(this.$data.user);
    loading.close();
  }
}
