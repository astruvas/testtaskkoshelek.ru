import {Component, Vue} from 'vue-property-decorator';
import {HistoryModule} from '@/core/store/modules/history';
import { getPagination } from '@/core/utils/render';
import { IPagination } from '@/core/interfaces/IPagination';

@Component({
  name: 'History'
})
export default class History extends Vue implements IPagination {
  public count = 1;
  public current = 1;
  public pageSize = 10;

  private collection: [] = [];
  private isHistory = false;

  public async mounted() {
    await this.getHistory();
  }

  private async getHistory(page: number = this.current) {
    this.collection = getPagination({current: page, pageSize: this.pageSize}, HistoryModule.data);
    this.count = HistoryModule.length;
    this.isHistory = HistoryModule.length > 0;
  }

  private async toChange(index: number) {
    this.current = index;
    await this.getHistory(index);
  }
}
