import {Component, Vue} from 'vue-property-decorator';
import {mapGetters, mapActions} from 'vuex';
import {UserModule} from '@/core/store/modules/user';
import {Loading, Input} from 'element-ui';
import {getPagination} from '@/core/utils/render';
import Collection from '@/components/collection/Collection.vue';
import { IPagination } from '@/core/interfaces/IPagination';
import { AlbumModule } from '@/core/store/modules/album';
import { IAlbum } from '@/core/interfaces/IAlbum';
import { FavoriteModule } from '@/core/store/modules/favorite';
import Favorites from '@/components/favorites/Favorites.vue';

@Component({
  name: 'Home',
  components: {
    Favorites
  },
  computed: {
    ...mapGetters({
      user: 'getUserById',
      pictures: 'getPictureById',
      search: 'getSearch',
      album: 'getAlbumById',
      removeAlbum: 'removeAlbumById',
      sortAlbum: 'getSortAlbumById'
    })
  }
})
export default class Home extends Vue implements IPagination {
  public user!: any;
  public pictures!: any;
  public search!: any;
  public album!: any;
  public removeAlbum!: any;
  public sortAlbum!: any;

  public count = 1;
  public current = 1;
  public pageSize = 10;

  private collection: [] = [];
  private isFavorites = false;
  private searchString = '';
  private isSearchString = false;

  public async mounted() {
    await this.getAlbums();
  }

  private async getAlbums(page: number = this.current) {
    const loading = Loading.service({ target: '.left-card' });
    this.count = AlbumModule.length;
    this.$data.collection = getPagination({current: page, pageSize: this.pageSize}, AlbumModule.data)
      .map((item: any) => {
        return {
          data: item,
          user: this.user(item.userId),
          pictures: this.pictures(item.id),
        };
      });
    this.isFavorites = FavoriteModule.length > 0;
    loading.close();
  }

  private async toChange(index: number) {
    this.current = index;
    await this.getAlbums(index);
  }

  private async toUser(index: number) {
    this.$router.push('/user/' + index);
  }

  private async toSearch(index: string) {
    this.$data.isSearchString = true;
    const loading = Loading.service({ fullscreen: true });
    if (index !== '') {
      this.collection = this.search(index);
    } else {
      this.$data.isSearchString = false;
      await this.getAlbums(this.current);
    }
    console.log(this.collection);
    loading.close();
  }

  private async toFavorites(model: any) {
    const loading = Loading.service({ target: '.left-card' });
    await FavoriteModule.Adding(this.album(model.row.data.id));
    this.getAlbums(this.current);
    loading.close();
  }
}
