import {Component, Vue} from 'vue-property-decorator';

@Component({
  name: 'Layout'
})
export default class Layout extends Vue {
  private isCollapse: boolean = false;

  public created() {
    window.addEventListener('resize', this.onResize);
    window.addEventListener('load', this.onResize);
  }

  private onResize() {
    if (window.innerWidth < 992) {
      this.isCollapse = true;
    } else {
      this.isCollapse = false;
    }
  }
}
