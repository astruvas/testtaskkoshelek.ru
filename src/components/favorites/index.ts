import { FavoriteModule } from '@/core/store/modules/favorite';
import {Component, PropSync, Vue} from 'vue-property-decorator';
import { mapGetters } from 'vuex';

@Component({
  name: 'Favorites',
  computed: {
    ...mapGetters({
      favorite: 'getFavoriteById'
    })
  }
})
export default class Favorites extends Vue {
  public favorite!: any;

  public collection = [];

  public async mounted() {
    await this.getAlbums();
  }

  private async getAlbums() {
    this.collection = FavoriteModule.data;
  }

  private async toFavorites(model: any) {
    const data = this.favorite(model.row.id);
    await FavoriteModule.Remove(data);
    await this.reload();
  }

  private async reload() {
    this.$emit('reload', console.log('Ok'));
  }
}
