export const getReverse = (data: any) => {
  return data.split('/').reverse()[1];
};

export const getPagination = (conf: any, data: any) => {
  if (data.length > 0) {
    const sort = data.sort();
    return sort.slice((conf.current - 1) * conf.pageSize, conf.current * conf.pageSize);
  }
};
