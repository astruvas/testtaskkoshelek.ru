import http from '@/core/utils/http';

export const getUsers = (id: number | string = '') =>
  http({
    url: '/users/' + id,
    method: 'get'
  });

export const getAlbums = (id: number | string = '') =>
  http({
    url: '/albums/' + id,
    method: 'get'
  });

export const getPictures = (id: number | string = '') =>
  http({
    url: '/photos/' + id,
    method: 'get'
  });
