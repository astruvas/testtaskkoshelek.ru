import axios from 'axios';
import {Notification} from 'element-ui';

const http = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: process.env.VUE_APP_TIMEOUT
});

http.interceptors.request.use((config) => {
    config.headers['Content-Type'] = 'application/json';

    return config;
  },
  (error) => {
    Notification({
      title: 'Error',
      message: error,
      type: 'error'
    });
  });

http.interceptors.response.use((response) => {
    if (response.status !== 200) {
      Notification({
        title: 'Error',
        message: response.data.message.toString(),
        type: 'error'
      });
      return Promise.reject(new Error(response.data.message || 'Unidentified error'));
    } else {
      return response;
    }
  },
  (error) => {
    const { status, statusText } = error.response;

    if (status === 500) {
      Notification({
        title: 'Error',
        message: status,
        type: 'error'
      });
    }

    Notification({
      title: statusText || 'Error',
      message: status,
      type: 'error'
    });
  });

export default http;
