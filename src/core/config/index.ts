import router from '@/core/router';
import {Route} from 'vue-router';
import nProgress from 'nprogress';
import {ISettings} from '@/core/interfaces/ISettings';
import 'nprogress/nprogress.css';
import { UserModule } from '../store/modules/user';
import { AlbumModule } from '../store/modules/album';
import { PictureModule } from '../store/modules/picture';

const settings: ISettings = {
  title: 'Test task',
  description: '',
  keywords: ''
};

nProgress.configure({ showSpinner: false });

const getPageTitle = (key: string) => {
  if (key) {
    return key + ' - ' + settings.title;
  }

  return settings.title;
};

const getPageDescription = (key: string) => {
  if (key) {
    return key;
  }

  return settings.description;
};

const getPageKeywords = (key: string) => {
  if (key) {
    return key;
  }

  return settings.keywords;
};

router.beforeEach(async (to: Route, _: Route, next: any) => {
  nProgress.start();

  try {
    if ((UserModule.data as any).__ob__.vmCount === 0) {
      await UserModule.Get();
      await AlbumModule.Get();
      await PictureModule.Get();
    }
    next();
  } catch (e) {
    console.error(e);
    nProgress.done();
  }

  nProgress.done();
});

router.afterEach((to: Route) => {
  nProgress.done();
  document.title = getPageTitle(to.meta?.title);
  document.querySelector('meta[name="description"]')?.setAttribute('content', getPageDescription(to.meta?.description));
  document.querySelector('meta[name="keywords"]')?.setAttribute('content', getPageKeywords(to.meta?.keywords));
});
