import store from '@/core/store';
import {Action, Module, Mutation, getModule, VuexModule} from 'vuex-module-decorators';
import {IFavoriteGetter, IFavorite} from '@/core/interfaces/IFavorite';
import { HistoryModule } from './history';

@Module({ dynamic: true, store, name: 'favorite' })
class Favorite extends VuexModule implements IFavorite {
  public data = [];
  public length = 0;

  @Action
  public async Adding(model: any) {
    const array = {
      item: model,
      operation: OperationType.adding
    };
    await this.SET_FAVORITE(array);
  }

  @Action
  public async Remove(model: any) {
    const array = {
      item: model,
      operation: OperationType.deletion
    };
    await this.SET_FAVORITE(array);
  }

  @Mutation
  private async SET_FAVORITE(model: any) {
    switch (model.operation) {
      case OperationType.adding:
        store.getters.addFavoriteByElement(model.item);
        store.getters.removeAlbumById(model.item.id);
        break;
      case OperationType.deletion:
        store.getters.addAlbumByElement(model.item);
        store.getters.removeFavoriteById(model.item.id);
        break;
    }
    this.length = store.state.favorite.data.length;
    this.data = store.state.favorite.data as any;
    await HistoryModule.SetHistory(model);
    console.log(store.state.history);
  }
}

export const FavoriteModule = getModule(Favorite);

export enum OperationType {
  adding,
  deletion
}
