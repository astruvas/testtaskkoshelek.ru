import store from '@/core/store';
import {Action, Module, Mutation, getModule, VuexModule} from 'vuex-module-decorators';
import {IPicture} from '@/core/interfaces/IPicture';
import {getPictures} from '@/core/utils/http/models';
import {Notification} from 'element-ui';

@Module({dynamic: true, store, name: 'picture'})
class Picture extends VuexModule implements IPicture {
  public data = {};
  public length = 1;

  @Action
  public async Get(id: string = '') {
    if (!id) {
      const {data} = await getPictures();

      if (!data) {
        throw Error('Data not found');
      }

      await this.SET_PICTURES(data);
      await this.SET_LENGTH(data);
    }
  }

  @Mutation
  private async SET_PICTURES(model: IPicture) {
    this.data = model;
  }

  @Mutation
  private async SET_LENGTH(int: number) {
    this.length = int;
  }
}

export const PictureModule = getModule(Picture);
