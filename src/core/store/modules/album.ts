import store from '@/core/store';
import {Action, Module, Mutation, getModule, VuexModule} from 'vuex-module-decorators';
import {IAlbum} from '@/core/interfaces/IAlbum';
import {getAlbums} from '@/core/utils/http/models';
import {Notification} from 'element-ui';

@Module({ dynamic: true, store, name: 'album' })
class Album extends VuexModule implements IAlbum {
  public data = {};
  public length = 1;

  @Action
  public async Get(id: string = '') {
    if (!id) {
      const {data} = await getAlbums();

      if (!data) {
        throw Error('Data not found');
      }

      await this.SET_ALBUMS(data);
    }
  }

  @Mutation
  private async SET_ALBUMS(model: IAlbum) {
    this.data = model;
    this.length = model.length;
  }
}

export const AlbumModule = getModule(Album);
