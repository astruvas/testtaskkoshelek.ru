import store from '@/core/store';
import {Action, Module, Mutation, getModule, VuexModule} from 'vuex-module-decorators';
import {IHistoryGetter, IHistory, IHistoryInfo} from '@/core/interfaces/IHistory';
import {Notification} from 'element-ui';

@Module({ dynamic: true, store, name: 'history' })
class History extends VuexModule implements IHistory {
  public data: IHistoryInfo[] = [];
  public length = 0;

  @Action
  public async SetHistory(model: any) {
    await this.SET_HISTORY(model);
  }

  @Mutation
  private async SET_HISTORY(model: any) {
    this.data.push({
      date: new Date().toLocaleString(),
      model: model.item,
      operationType: model.operation
    });
    this.length = store.state.history.data.length;
  }
}

export const HistoryModule = getModule(History);
