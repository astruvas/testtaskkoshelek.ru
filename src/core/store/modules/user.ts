import store from '@/core/store';
import {Action, Module, Mutation, getModule, VuexModule} from 'vuex-module-decorators';
import {IUser} from '@/core/interfaces/IUser';
import {getUsers} from '@/core/utils/http/models';
import {HistoryModule} from '@/core/store/modules/history';
import {Notification} from 'element-ui';

@Module({ dynamic: true, store, name: 'user' })
class User extends VuexModule implements IUser {
  public data = {};
  public length = 1;

  @Action
  public async Get(id: string = '') {
    if (!id) {
      const {data} = await getUsers();

      if (!data) {
        throw Error('Data not found');
      }

      await this.SET_USERS(data);
      await this.SET_LENGTH(data.length);
    }
  }

  @Mutation
  private async SET_USERS(model: IUser) {
    this.data = model;
  }

  @Mutation
  private async SET_LENGTH(int: number) {
    this.length = int;
  }



  // @Action
  // public async GetPeoples(model: string | (string | null)[] | number) {
  //   await getPeoples(model)
  //     .then(response => {
  //       this.SET_PEOPLES(response);
  //     })
  //     .catch(error => {
  //       console.log(error);
  //       Notification({
  //         title: 'Error',
  //         message: error,
  //         type: 'error'
  //       });
  //     });
  // }
  //
  // @Action
  // public async GetPeople(model: string | (string | null)[] | number) {
  //   await getPeople(model)
  //     .then(response => {
  //       this.SET_PEOPLE(response);
  //     })
  //     .catch(error => {
  //       Notification({
  //         title: 'Error',
  //         message: error,
  //         type: 'error'
  //       });
  //     });
  // }
  //
  // @Action
  // public async GetSearch(model: any) {
  //   await this.GET_SEARCH(model);
  // }
  //
  // @Action
  // public async SetFavorites(model: any) {
  //   await this.SET_FAVORITES(model)
  //   Notification({
  //     title: 'Success',
  //     message: 'Ok',
  //     type: 'success'
  //   });
  // }
  //
  // @Action
  // public async RemoveFavorites(model: any) {
  //   await this.REMOVE_FAVORIES(model);
  //   Notification({
  //     title: 'Success',
  //     message: 'Ok',
  //     type: 'success'
  //   });
  // }
  //
  // @Mutation
  // private async SET_PEOPLES(model: any) {
  //   this.peoples = await model.data.results;
  //   this.buffer = [...this.peoples];
  //   if (this.favorites.length !== 0) {
  //     this.peoples.map((o: any, index: number) =>
  //       this.favorites.some((a: any) => {
  //         if (+a.url.split('/').reverse()[1] === +o.url.split('/').reverse()[1]) {
  //           this.peoples.splice(index, 1);
  //         }
  //       }));
  //
  //     this.buffer = [...this.peoples];
  //   }
  //
  //   this.count = await model.data.count;
  //   this.next = await model.data.next;
  //   this.prev = await model.data.previous;
  // }
  //
  // @Mutation
  // private async SET_PEOPLE(model: any) {
  //   this.people = model.data;
  // }
  //
  // @Mutation
  // private async GET_SEARCH(model: string) {
  //   let data = model !== '' ? this.peoples.filter((a: any) => {
  //       return (a.name.toLowerCase().indexOf(model.toLowerCase()) === 0);
  //     }
  //   ) : this.buffer;
  //
  //   this.peoples = data;
  // }
  //
  // @Action
  // private async createFilter(model: any) {
  //
  // }
  //
  // @Mutation
  // private async SET_FAVORITES(model: any) {
  //   this.favorites.push(this.peoples[model]);
  //   let operation = {
  //     model: this.peoples[model],
  //     operation: 0
  //   };
  //
  //   await HistoryModule.SetHistory(operation);
  // }
  //
  // @Mutation
  // private async REMOVE_FAVORIES(model: any) {
  //   this.favorites.splice(model, 1);
  //
  //   let operation = {
  //     model: this.peoples[model],
  //     operation: 1
  //   };
  //
  //   await HistoryModule.SetHistory(operation);
  // }
}

export const UserModule = getModule(User);
