import Vue from 'vue';
import Vuex from 'vuex';
import {IRoot} from '@/core/interfaces/IRoot';
import {IAlbumGetter, IAlbumInfo} from '../interfaces/IAlbum';
import {IUserGetter, IUserInfo} from '../interfaces/IUser';

Vue.use(Vuex);

export default new Vuex.Store<IRoot>({
  getters: {
    getUserById: (state) => (id: number) => {
      return state.user.data.find((i) => i.id === id);
    },
    getAlbumById: (state) => (id: number) => {
      return state.album.data.find((i) => i.id === id);
    },
    getPictureById: (state) => (id: number) => {
      return state.picture.data.filter((i) => i.albumId === id);
    },
    getFavoriteById: (state) => (id: number) => {
      return state.favorite.data.find((i: any) => i.id === id);
    },
    getSearch: (state) => (index: string) => {
      const albums = state.album.data
        .filter((i) => (i.title.toLowerCase().indexOf(index.toLowerCase()) > -1))
        .sort((a: any, b: any) => a.title < b.title ? -1 : 1);
      const pictures = state.picture.data
        .filter((i) => (i.title.toLowerCase().indexOf(index.toLowerCase()) > -1))
        .sort((a: any, b: any) => a.title < b.title ? -1 : 1);
      const data = albums
        .map((item: any) => {
          return {
            id: item.id,
            title: item.title,
            pictures: state.picture.data
              .filter((i) => (i.title.toLowerCase().indexOf(index.toLowerCase()) === 0)
                && i.albumId === item.id)
              .sort((a: any, b: any) => a.title < b.title ? -1 : 1)
          };
        });
      return data;
    },
    removeAlbumById: (state) => (id: number) => {
      console.log(id);
      const index: number = state.album.data.findIndex((i) => i.id === id);
      return state.album.data.splice(index, 1);
    },
    removeFavoriteById: (state) => (id: number) => {
      const index = state.favorite.data.findIndex((i: any) => i.id === id);
      return state.favorite.data.splice(index, 1);
    },
    addFavoriteByElement: (state) => (model: any) => {
      return state.favorite.data.push(model);
    },
    addAlbumByElement: (state) => (model: any) => {
      state.album.data.push(model);
      const aIndex = state.album.data.findIndex((i: any) => i.id === model.id);
      const xIndex = state.album.data.findIndex((i: any) => i.id > model.id);
      const data = state.album.data.splice(xIndex, 0, state.album.data.splice(aIndex, 1)[0]);
      return data;
    }
  }
});
