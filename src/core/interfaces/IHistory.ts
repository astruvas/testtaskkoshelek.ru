export interface IHistory {
  data: any;
  length: number;
}

export interface IHistoryGetter {
  data: IHistory[];
}

export interface IHistoryInfo {
  date: string;
  operationType: number;
  model: any;
}
