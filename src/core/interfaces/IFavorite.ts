export interface IFavorite {
  data: any;
}

export interface IFavoriteGetter {
  data: IFavoriteInfo[];
  element: number;
}

export interface IFavoriteInfo {
  id: number;
  userId: number;
  title: string;
}
