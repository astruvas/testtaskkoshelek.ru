import {IPagination} from '@/core/interfaces/IPagination';

export interface IUser {
  data: any;
  length: number;
}

export interface IUserGetter {
  data: IUserInfo[];
}

export interface IUserInfo extends IAddress, ICompany {
  id: number;
  name: string;
  username: string;
  email: string;
  address: IAddress;
  phone: string;
  website: string;
  company: ICompany;
}

export interface IAddress extends IGeo {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: IGeo;
}

export interface IGeo {
  lat: string;
  lng: string;
}

export interface ICompany {
  name: string;
  catchPhrase: string;
  bs: string;
}
