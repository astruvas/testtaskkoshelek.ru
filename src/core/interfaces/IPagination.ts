export interface IPagination {
  count: number;
  pageSize: number;
  current: number;
}
