export interface IPicture {
  data: any;
  length: number;
}

export interface IPictureGetter {
  data: IPictureInfo[];
}

export interface IPictureInfo {
  id: number;
  albumId: number;
  title: string;
  url: string;
  thumbnailUrl: string;
}
