export interface IAlbum {
  data: any;
  length: number;
}

export interface IAlbumGetter {
  data: IAlbumInfo[];
}

export interface IAlbumInfo {
  id: number;
  userId: number;
  title: string;
}
