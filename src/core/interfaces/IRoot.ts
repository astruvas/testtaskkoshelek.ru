import {IUserGetter} from './IUser';
import {IAlbumGetter} from './IAlbum';
import {IPictureGetter} from './IPicture';
import {IHistory} from './IHistory';
import {IFavoriteGetter} from './IFavorite';

export interface IRoot {
  user: IUserGetter;
  album: IAlbumGetter;
  picture: IPictureGetter;
  favorite: IFavoriteGetter;
  history: IHistory;
}
