export interface ISettings {
  title: string;
  keywords: string;
  description: string;
}
