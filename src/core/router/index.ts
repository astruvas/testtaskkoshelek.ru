import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/home/Home.vue'),
    meta: {
      title: 'Home',
      description: 'Home page in Test task',
      keywords: 'index keys from Home page'
    }
  },
  {
    path: '/history',
    name: 'History',
    component: () => import('@/views/history/History.vue'),
    meta: {
      title: 'History',
      description: 'History page in Test task',
      keywords: 'index keys from History page'
    }
  },
  {
    path: '/user/:id',
    name: 'User',
    component: () => import('@/views/user/User.vue'),
    meta: {
      title: 'User',
      description: '',
      keywords: ''
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
