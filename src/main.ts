import Vue from 'vue';
import App from '@/views/layout/Layout.vue';
import router from '@/core/router';
import store from '@/core/store';

import '@/plugins';
import '@/core/config';

import '@/style/index.scss';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
